# README Flow - Grid System #

This README is for my custom grid system. If you have any questions please email contact@thomasgwebdesign.com

### What is this repository for? ###

* This repository is for my "Flow" Grid System. You use classes to position where you want your information to be.
* 0.1
* [Author's Website](http://www.thomasgwebdesign.com)

### How do I get set up? ###

* Create a div with the class "row"
* Then in side of there create more divs with the classes "col-1, col-2, col-3, col-4, col-5, etc.." all the way to "col-12".
* There are 12 invisible columns. So this means when you make those divs that they need to add up to 12. For example: <div class="**col-4**"></div><div class="**col-8**"></div>
* For now the proportions for medium devices is supported but does not work the way I want it to. To use these it is "col-m-1" to "col-m-12"
* If there are any bugs please report them to contact@thomasgwebdesign.com

### Contribution guidelines ###

* 
* 
* 

### Who do I talk to? ###

