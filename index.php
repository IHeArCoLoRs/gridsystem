<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Flow - The Light Grid System</title>
	<link rel="stylesheet" href="styles/flowStyle.css">
</head>
<body>
	<div class="header">
		<div>Flow Grid System</div> 
	</div>
	<div class="row">
		<div class="col-3">
			<nav>
				<ul>
					<a href="index.php"><li>Home</li></a>
					<a href="#/">
						<li>Login</li>
					</a>
					<a href="#/">
						<li>Portfolio</li>
					</a>
					<a href="#/">
						<li>Contact Us</li>
					</a>
				</ul>
			</nav>
		</div>
		<div class="col-3"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, mollitia voluptate iusto consectetur ipsam laborum nostrum officia dicta enim? Tenetur!</p></div>
		<div class="col-6"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui, mollitia voluptate iusto consectetur ipsam laborum nostrum officia dicta enim? Tenetur!</p></div>
	</div>

<div class="row panel">
	<div class="col-8 col-m-12"><p style="background-color: #FF00A8;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto fuga voluptatum nulla soluta rerum sed eaque, rem qui libero quia explicabo magni laudantium, deleniti delectus voluptates atque nostrum, itaque debitis voluptate! Amet eum velit rem.</p></div>
	<div class="col-4 col-m-12" ><p style="background-color: #00FFD8;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam animi pariatur, aliquam ut hic repellendus.</p></div>
</div>






</body>
</html>